Welcome to the Enhancement custom theme!

Enhancement is a theme for custom ROMs having the CM13 theme engine. After 3 dark themes, I switched to a white theme for 2 reasons: lack of time to support a dark theme and I bought a white skin for my Nexus 5. This has inspired me...
The aim of Enhancement is to provide a stock UI with enhanced icons and colors for your device. I want to keep it light and clean.

[XDA Developers thread](http://forum.xda-developers.com/android/themes/cm13-free-light-theme-enhancement-floss-t3370870)

## BASIC & SYSTEM-WIDE FEATURES ##

*  Framework (animations, white navbar, inverted icons on the statusbar...)<br>
*  System settings (Dirty Tweaks from DU are supported)<br>
*  SystemUI (Android N notifications)<br>
*  Bootanimation<br>
*  Wallpapers (launcher & lockscreen)<br>
*  Font (Ubuntu)<br>
*  Icon pack<br>
*  Sounds: ringtone, alarm and notification<br>


## THEMED APPS ##

*  AdAway<br>
*  AFWall+<br>
*  AICP OTA<br>
*  AOSP Calculator<br>
*  AOSP Contacts<br>
*  AOSP Dialer<br>
*  K-9 Mail<br>
*  LMT Launcher<br>
*  Package Installer<br>
*  Theme Chooser (including Cyngn version)<br>

I'll add new apps (as soon as the basic features are stabilised), but <b>only free/libre and open source apps</b>.


#### SCREENSHOTS ####

![enhancement theme](http://i.imgur.com/xZhkhJK.png)


#### DOWNLOADS ####

[Androidfilehost](https://www.androidfilehost.com/?w=files&flid=55126)
[Via XDA Labs app](https://labs.xda-developers.com/store/app/com.primokorn.enhancement)
[F-Droid](https://f-droid.org/repository/browse/?fdfilter=enhancement&fdid=com.primokorn.enhancement) - Updates not released by me


#### INSTALLATION ####

Enhancement should work on any device running Marshmallow and CM13 theme engine (minimum: hdpi resolution is recommended for better quality). Tablets are not officially supported.

1. Be sure to have a compatible custom ROM
2. Download the apk file and install it
3. A notification will appera into your notification panel. Click on it to be redirected to Settings > Themes > Enhancement.
4. Check the features you want to apply and confirm.
5. Reboot.


#### HOW TO REPORT A BUG ####

*  <b>Visual bug</b> (e.g missing icon)<br>
Mention the app (and your custom rom in case of system icons) and post a screenshot. Upload the related apk if you can (optional, to save me time).

*  <b>FC?</b><br>
[Record a log](http://forum.xda-developers.com/android/help/guide-easiest-to-utilize-catlog-t2865994) when the issue occurs (CatLog, MatLog, adb...). Post the steps to reproduce the bug.

*  Thanks.


#### LICENSES ####

    Enhancement theme is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License version 3 as published by
    the Free Software Foundation.

    Enhancement theme is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Enhancement theme.  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses).

Some custom icons are designed by Freepik.


### <a name="copyright"></a>CyanogenMod Copyright Header ###

     Copyright (C) 2015 The CyanogenMod Project

     Licensed under the Apache License, Version 2.0 (the "License");
     you may not use this file except in compliance with the License.
     You may obtain a copy of the License at

          http://www.apache.org/licenses/LICENSE-2.0

     Unless required by applicable law or agreed to in writing, software
     distributed under the License is distributed on an "AS IS" BASIS,
     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     See the License for the specific language governing permissions and
     limitations under the License.